## Downloading the files

You can get the project's files and database [here](https://bitbucket.org/leeconnelly/mcg-technical-test/downloads/)

## Building

You will need to install [node](https://nodejs.org/en/) in order to run this project.

Inside the project's theme directory (e.g E:\www\mcg\wp-content\themes\mcg) run this command:  
`npm i`

Below are the available scripts you can run for this project.

To build the assets for development you can run this command:  
`npm run dev`

To rebuild assets whenever you make a change you can run this command:  
`npm run watch`

To build the assets for production you can run this command:  
`npm run prod`
