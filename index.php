<!DOCTYPE html>
<html class="h-full" lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head() ?>
</head>
<body class="h-full antialiased leading-none text-gray-900 font-body bg-gradient-to-b from-gray-100 to-blue-100" x-data="{ open: false }">

    <?php while(have_posts()): the_post() ?>

    <header class="fixed top-0 left-0 right-0 z-20 flex items-center justify-between p-4 xl:pt-8 xl:pl-40">
        <a class="xl:mt-1" href="<?php echo get_site_url() ?>">
            <img class="w-48" src="<?php echo get_theme_file_uri('src/images/logo.svg') ?>" alt="MCG | Human Engagement logo">
        </a>
        <button class="xl:hidden" @click="open = !open">
            <div class="w-8 bg-black h-2px"></div>
            <div class="w-6 mt-2 ml-auto bg-black h-2px"></div>
        </button>
    </header>

    <aside class="fixed top-0 bottom-0 right-0 z-10 w-full px-4 pt-24 pb-6 transition-transform duration-300 transform translate-x-full bg-gray-200" :class="{ 'translate-x-full delay-300': !open }">
        <div class="flex flex-col justify-between h-full">
            <ul>
                <li>
                    <a class="tracking-widest uppercase transition-opacity duration-300" :class="{ 'delay-300': open }" x-show.transition.opacity.duration.300ms="open" href="#!">About MCG</a>
                </li>
                <li class="pt-4">
                    <a class="tracking-widest uppercase transition-opacity duration-300" :class="{ 'delay-400': open }" x-show.transition.opacity.duration.300ms="open" href="#!">Our Brands</a>
                </li>
                <li class="pt-4">
                    <a class="tracking-widest uppercase transition-opacity duration-300" :class="{ 'delay-500': open }" x-show.transition.opacity.duration.300ms="open" href="#!">Explore Careers</a>
                </li>
            </ul>
            <ul class="flex">
                <li :class="{ 'delay-300': open }" x-show.transition.opacity.duration.300ms="open">
                    <a href="#!">
                        <img src="<?php echo get_theme_file_uri('src/images/facebook.svg') ?>" alt="MCG's Facebook profile">
                    </a>
                </li>
                <li class="pl-4" :class="{ 'delay-400': open }" x-show.transition.opacity.duration.300ms="open">
                    <a href="#!">
                        <img src="<?php echo get_theme_file_uri('src/images/instagram.svg') ?>" alt="MCG's Instagram profile">
                    </a>
                </li>
                <li class="pl-4" :class="{ 'delay-500': open }" x-show.transition.opacity.duration.300ms="open">
                    <a href="#!">
                        <img src="<?php echo get_theme_file_uri('src/images/twitter.svg') ?>" alt="MCG's Twitter profile">
                    </a>
                </li>
            </ul>
        </div>
    </aside>

    <aside class="fixed top-0 bottom-0 left-0 z-30 hidden w-32 px-8 pt-10 pb-24 border-r border-gray-400 xl:block">
        <div class="flex flex-col items-center justify-between h-full pt-2">
            <button>
                <div class="w-8 bg-black h-2px"></div>
                <div class="w-6 mt-2 bg-black h-2px"></div>
            </button>
            <div class="w-40 tracking-widest uppercase transform rotate-90">
                <div class="flex items-center pl-2">
                    <div class="flex-grow flex-shrink-0 inline-block w-10 bg-gray-900 h-2px"></div>
                    <div class="inline-block pl-3 text-right">Scroll</div>
                </div>
                <div class="pt-4 text-right">to navigate</div>
            </div>
        </div>
    </aside>

    <aside class="fixed top-0 bottom-0 right-0 z-30 hidden w-32 px-8 py-10 border-l border-gray-400 xl:block">
        <div class="flex flex-col items-center justify-between h-full">
            <a class="text-2xl font-bold" href="#!">En</a>
            <div class="flex-shrink-0 w-56 tracking-widest text-center uppercase transform rotate-90">Welcome to MCG</div>
            <ul>
                <li>
                    <a href="#!">
                        <img src="<?php echo get_theme_file_uri('src/images/facebook.svg') ?>" alt="MCG's Facebook profile">
                    </a>
                </li>
                <li class="pt-6">
                    <a href="#!">
                       <img src="<?php echo get_theme_file_uri('src/images/instagram.svg') ?>" alt="MCG's Instagram profile">
                    </a>
                </li>
                <li class="pt-6">
                    <a href="#!">
                       <img src="<?php echo get_theme_file_uri('src/images/twitter.svg') ?>" alt="MCG's Twitter profile">
                    </a>
                </li>
            </ul>
        </div>
    </aside>

    <main class="relative flex items-center w-full h-full">
        <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, ['class' => 'absolute bottom-0 left-0 right-0 object-cover w-full h-full opacity-25 xl:opacity-100 opacity top-20 big-image']) ?>

        <?php $i = 0; while (have_rows('slides')): the_row(); ?>
            <div class="container relative px-4 mx-auto xl:pt-40 content <?php echo $i !== 0 ? 'hidden' : '' ?>" data-slide="<?php echo $i ?>">
                <div class="xl:w-1/2">
                    <?php the_sub_field('content') ?>

                    <?php if (get_sub_field('link_text') && get_sub_field('link_url')): ?>
                        <a class="inline-flex items-center mt-8 xl:mt-24" href="<?php the_sub_field('link_url') ?>">
                            <svg width="32" height="32" viewBox="0 0 512 512"><path d="M506.134 241.843l-.018-.019-104.504-104c-7.829-7.791-20.492-7.762-28.285.068-7.792 7.829-7.762 20.492.067 28.284L443.558 236H20c-11.046 0-20 8.954-20 20s8.954 20 20 20h423.557l-70.162 69.824c-7.829 7.792-7.859 20.455-.067 28.284 7.793 7.831 20.457 7.858 28.285.068l104.504-104 .018-.019c7.833-7.818 7.808-20.522-.001-28.314z"/></svg>
                            <div class="pl-5 tracking-widest uppercase">
                                <?php the_sub_field('link_text') ?>
                            </div>
                        </a>
                    <?php endif; ?>
                    
                </div>
            </div>
        <?php $i++; endwhile; ?>

        <div class="absolute left-0 right-0 hidden w-4/5 h-full grid-cols-3 mx-auto pointer-events-none xl:grid">
            <div class="w-px h-full mx-auto bg-gray-400 line"></div>
            <div class="w-px h-full mx-auto bg-gray-400 line"></div>
            <div class="w-px h-full mx-auto bg-gray-400 line"></div>
        </div>
    </main>

    <nav class="fixed bottom-0 left-0 right-0 xl:pr-40">
        <ul class="relative flex justify-between max-w-xl px-4 pb-5 mx-auto xl:pb-6 xl:w-1/2 xl:mr-0 xl:max-w-none" data-steps>
            <li class="leading-snug tracking-widest text-right uppercase transition-opacity duration-300 text-md" data-step="0" data-active="true">
                <a class="flex items-center" href="#!">
                    <h2>
                        About
                        <div class="hidden xl:block">MCG</div>
                    </h2>
                    <div class="hidden pl-5 text-5xl xl:block">01</div>
                </a>
            </li>
            <li class="leading-snug tracking-widest text-right uppercase transition-opacity duration-300 opacity-50 text-md" data-step="1">
                <a class="flex items-center" href="#!">
                    <h2>
                        <div class="hidden xl:block">Our</div>
                        Brands
                    </h2>
                    <div class="hidden pl-5 text-5xl xl:block">02</div>
                </a>
            </li>
            <li class="leading-snug tracking-widest text-right uppercase transition-opacity duration-300 opacity-50 text-md" data-step="2">
                <a class="flex items-center" href="#!">
                    <h2>
                        <div class="hidden xl:block">Explore</div>
                        Careers
                    </h2>
                    <div class="hidden pl-5 text-5xl xl:block">03</div>
                </a>
            </li>
        </ul>
    </nav>

    <div class="fixed bottom-0 left-0 hidden w-px transition-transform duration-300 bg-gray-900 h-2px" data-line style="height: 4px;"></div>

    <?php endwhile; wp_footer() ?>
    
</body>
</html>