<?php

add_theme_support( 'title-tag' );

add_theme_support( 'post-thumbnails' );

add_action('wp_enqueue_scripts', 'add_scripts');

function add_scripts()
{
    wp_enqueue_style('style', get_theme_file_uri('build/app.css'));
    wp_enqueue_script('script', get_theme_file_uri('build/app.js'), [], false, true);
}