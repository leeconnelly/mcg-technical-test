const { colors } = require('tailwindcss/defaultTheme')

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: ['./**/*.php'],
  theme: {
    fontFamily: {
      'body': ['gilroy', 'sans-serif'],
    },
    extend: {
      height: {
        '2px': '2px'
      },
      letterSpacing: {
        wide: '0.1em',
        widest: '0.3em',
      },
      colors: {
        gray: {
          ...colors.gray,
          '100': '#ebf1f1',
        },
        blue: {
          ...colors.blue,
          '100': '#f2f6f7',
        }
      },
      transitionDelay: {
        '400': '400ms',
      }
    },
  },
  variants: {},
  plugins: [],
}
