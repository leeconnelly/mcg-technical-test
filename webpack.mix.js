const mix = require('laravel-mix')

mix.js('src/js/app.js', 'build/app.js')
  .postCss('src/css/app.css', 'build/app.css', [
    require('tailwindcss'),
  ])
  .setPublicPath('build')
  .setResourceRoot('./')
  .version()