import 'alpinejs'

const line = document.querySelector('[data-line]')
const activeStep = document.querySelector('[data-active="true"]')
const { x, width } = activeStep.getBoundingClientRect()

line.style.transform = `translateX(${x + (width / 2)}px) scaleX(${width})`
line.classList.remove('hidden')

document.querySelectorAll('[data-step]').forEach(step => {
    step.addEventListener('click', function() {

        document.querySelectorAll('[data-slide]').forEach(slide => {
            slide.classList.add('hidden')
        })

        document.querySelector(`[data-slide="${this.getAttribute('data-step')}"]`).classList.remove('hidden')

        document.querySelectorAll('[data-step]').forEach(step => {
            step.setAttribute('data-active', false)
            step.classList.add('opacity-50')
        })

        this.setAttribute('data-active', true)
        this.classList.remove('opacity-50')

        const { x, width } = this.getBoundingClientRect()
        line.style.transform = `translateX(${x + (width / 2)}px) scaleX(${width})`
    })
})